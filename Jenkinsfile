def get_branch() {
    script {
        return sh(script : 'echo ${GIT_BRANCH} | cut -d / -f 2', returnStdout: true).trim()
    }
}

pipeline {
    agent any

     tools{
         'org.jenkinsci.plugins.docker.commons.tools.DockerTool' 'docker'
     }

     environment {
 		PROJECT_VERSION = readMavenPom().getVersion()
		PROJECT_ARTIFACT_ID = readMavenPom().getArtifactId()
 		PROJECT_GROUP_ID = readMavenPom().getGroupId()
 		BRANCH_NAME = get_branch()
     }

    stages{
        stage('Checkout'){
            steps{
                checkout scm
            }
        }

        stage('Tests'){
            steps{
                withMaven(jdk: 'Jdk8', maven: 'maven-3.6.3') {
                    sh label: 'tests', script: './mvnw clean test'
                }
            }
        }

        stage('Analyse de la qualité'){
            steps{
                withMaven(jdk: 'Jdk8', maven: 'maven-3.6.3') {
                    withSonarQubeEnv('dev-sonarqube'){
                        sh label: 'static code analysis', script: 'mvn clean package sonar:sonar -Dsonar.projectName=countries/${BRANCH_NAME} -Dsonar.projectKey=countries:${BRANCH_NAME}'
                    }
                }
            }
        }

        stage('Packaging'){
            steps{
                withMaven(jdk: 'Jdk8', maven: 'maven-3.6.3') {
                    sh './mvnw clean package -Dskip.tests=true'
                }
            }
        }

        stage('Image docker du latest en Construction...'){
            when{
                expression { BRANCH_NAME == 'master' }
            }
            steps{
                script {
                    withDockerRegistry(credentialsId: 'myut-registry-cred', toolName: 'docker', url: 'https://registry.myurbantrip.com') {
                        sh label: 'Docker build', script: 'docker build -t registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:${PROJECT_VERSION} --build-arg JAR_FILE=target/${PROJECT_ARTIFACT_ID}-${PROJECT_VERSION}.jar .';
                        sh label: 'Push sur registry current version myurbantrip', script: 'docker push registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:${PROJECT_VERSION}'
                        sh label: 'Creation tag sur registry myurbantrip', script: 'docker tag registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:${PROJECT_VERSION} registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:latest'
                        sh label: 'Push sur registry latest myurbantrip', script: 'docker push registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:latest'
                        sh label: 'suppression de l\'image docker créee', script: 'docker image rm registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:${PROJECT_VERSION}'
                    }
                }
            }
        }

        stage('Image docker Construction...'){
            when {
                anyOf { branch pattern: 'feature*' }
             }
             steps{
                script {
                    withDockerRegistry(credentialsId: 'myut-registry-cred', toolName: 'docker', url: 'https://registry.myurbantrip.com') {
                        sh label: 'Docker build', script: 'docker build -t registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:${PROJECT_VERSION} --build-arg JAR_FILE=target/${PROJECT_ARTIFACT_ID}-${PROJECT_VERSION}.jar .';
                        sh label: 'Push sur registry current version myurbantrip', script: 'docker push registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:${PROJECT_VERSION}'
                        sh label: 'Creation tag sur registry myurbantrip', script: 'docker tag registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:${PROJECT_VERSION} registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:latest'
                        sh label: 'Push sur registry latest myurbantrip', script: 'docker push registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:latest'
                        sh label: 'suppression de l\'image docker créee', script: 'docker image rm registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:${PROJECT_VERSION}'
                    }
                }
             }
         }

        stage('Image docker deployment'){
             steps{
                  sh "docker service rm ${PROJECT_ARTIFACT_ID}"
                  sh "docker service create \
                        --name ${PROJECT_ARTIFACT_ID} \
                        --label com.docker.stack.namespace='devops' \
                        --network devops_web_network \
                        --constraint node.role==manager \
                        --container-label com.docker.stack.namespace='devops' \
                        --replicas 1 \
                        --publish published=8000,target=8080 \
                        registry.myurbantrip.com/${PROJECT_ARTIFACT_ID}:latest"
             }
         }

/*
        stage('Image docker Construction...'){
         	when {
         		 expression { BRANCH_NAME == 'master' }
             }
             steps{
             	script {
	            	docker.withRegistry('https://registry.myurbantrip.com', 'myut-registry-cred'){
 	            		def dockerImage = docker.build("${PROJECT_ARTIFACT_ID}","--build-arg JAR_FILE=target/${PROJECT_ARTIFACT_ID}-${PROJECT_VERSION}.jar .")
 	            		dockerImage.push('${PROJECT_VERSION}')
                        dockerImage.push('latest')
 	            	}
 				}
             }
         }
*/

    }

    post{
        always{
            echo 'Cleaning the workspace ...'
            cleanWs notFailBuild: true
        }
    }

}
